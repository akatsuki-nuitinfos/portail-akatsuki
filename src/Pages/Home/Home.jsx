import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import Toggle from "react-toggle";
import {DarkToggle} from "../../Components/DarkToggle";

function HomePage() {
    const users = useSelector(state => state.users);
    const user = useSelector(state => state.authentication.user);
    const dispatch = useDispatch();

    return (
        <div className="container">
            <div className="row mb-3">
                <div className="col-12">
                    <h1>Nos applications</h1>
                </div>
            </div>

            <div className="row main">

                <div className="col-lg-4 col-md-6 col-sm-6">
                    <div className="row justify-content-center h-100">
                        <a href="http://146.59.248.65:7000/" className="col-12 app-card h-100">
                            <div className={"h-100"}>
                                <div className="card text-white bg-gradient-lightblue h-100">
                                    <div className="card-body card-body d-flex justify-content-between align-items-center">
                                        <div>
                                            <h2 className="text-lg">Surfride</h2>
                                            <div>L'appli de réference pour les surfeurs...</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>


                <div className="col-sm-6 col-lg-8">
                    <div className="row">
                        <a href="http://146.59.248.65:7000/swagger-ui/index.html" className="col-sm-12 col-lg-4 col-md-12 app-card">
                            <div>
                                <div className="card text-white bg-gradient-blue">
                                    <div className="card-body card-body d-flex justify-content-between align-items-center">
                                        <div>
                                            <h2 className="text-lg">Swagger</h2>
                                            <div>Pour bien comprendre l'api</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <a href="http://146.59.248.65:8001/" className="col-sm-12 col-lg-4 col-md-12 app-card">
                            <div>
                                <div className="card text-white bg-gradient-blue">
                                    <div className="card-body card-body d-flex justify-content-between align-items-center">
                                        <div>
                                            <h2 className="text-lg">CSV_Data</h2>
                                            <div>Analyse de données</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <a href="http://146.59.248.65:8000/" className="col-sm-12 col-lg-4 col-md-12 app-card">
                            <div>
                                <div className="card text-white bg-gradient-blue">
                                    <div className="card-body card-body d-flex justify-content-between align-items-center">
                                        <div>
                                            <h2 className="text-lg">Calendar-API</h2>
                                            <div>API Calendrier</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <a href="http://146.59.248.65:8000/swagger-ui/index.html" className="col-sm-12 col-lg-4 col-md-12 app-card">
                            <div>
                                <div className="card text-white bg-gradient-blue">
                                    <div className="card-body card-body d-flex justify-content-between align-items-center">
                                        <div>
                                            <h2 className="text-lg">Swagger API Calendrier</h2>
                                            <div>Swagger</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>

                        <a href="https://gitlab.com/akatsuki-nuitinfos/csv-parser" className="col-sm-12 col-lg-4 col-md-12 app-card">
                            <div>
                                <div className="card text-white bg-gradient-blue">
                                    <div className="card-body card-body d-flex justify-content-between align-items-center">
                                        <div>
                                            <h2 className="text-lg">Repo rendu analyse de données</h2>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    );
}

export { HomePage };
import React,  { useState, useEffect } from "react";
import Toggle from "react-toggle";
import { useMediaQuery } from "react-responsive";

function DarkToggle() {
    const systemPrefersDark = useMediaQuery({
            query: "(prefers-color-scheme: dark)"
        },
        undefined,
        prefersDark => {
            setIsDark(prefersDark);
        });
    const [isDark, setIsDark] = useState(true);

    useEffect(() => {
        // whatever we put here will run whenever `isDark` changes
    }, [isDark]);

    return (
        <Toggle
            className="DarkToggle"
            checked={isDark}
            onChange={event => setIsDark(event.target.checked)}
            icons={{ checked: "🌙", unchecked: "🔆" }}
            aria-label="Dark mode"
        />
    );
}

export { DarkToggle };
